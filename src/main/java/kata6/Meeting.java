package kata6;

//import com.sun.deploy.util.StringUtils;


import java.util.*;
import java.util.stream.Collectors;

public class Meeting {

    public static String meeting(String s) {

        String[] splitedStr = s.split(";");
        StringBuilder sb = new StringBuilder();

        List<String> listOfNames = new ArrayList<>();

        for (int i = 0; i < splitedStr.length; i++) {

            String s1 = splitedStr[i];
            String replace = s1.replace(":", ",");
            String personName = replace.replaceFirst("(.*),(.*)", "$2,$1");

            listOfNames.add(i,"("+ personName+")");

        }

        listOfNames.sort(String.CASE_INSENSITIVE_ORDER);
        //java.util.Collections.sort(listOfNames);
        //String result = StringUtils.join(listOfNames, "").toUpperCase();
        String result = String.join("", listOfNames).toUpperCase();
        System.out.println(result);

        return result;
    }

    public static String meeting1(String s) {


        String personName = s.replaceFirst("(.*), (.*)", "$2 $1");
        System.out.println(personName);

        return null;

    }
    public static String streamMeeting(String s){

        return Arrays.stream(s.toUpperCase().split(";"))
                .map(guest -> guest.replaceAll("(\\w+):(\\w+)", "($2, $1)"))
                .sorted()
                .collect(Collectors.joining(""));

    }


}
