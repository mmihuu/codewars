package kata6;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

public class MexicanWave {

    public static String[] wave(String str) {

        str = str.toLowerCase();
        String[] text = new String[str.length()];
        for (int j = 0; j < str.length(); j++) {
            StringBuilder newSb = new StringBuilder(str);
            if (Character.isLowerCase(str.charAt(j))) {
                char c = Character.toUpperCase(str.charAt(j));
                newSb.setCharAt(j, c);
                text[j] = newSb.toString();
            }
        }
        String[] cleanedArray = Arrays.stream(text)
                .filter(Objects::nonNull)
                .toArray(String[]::new);

        return cleanedArray;
    }


    public static String[] wave1(String str) {
        return IntStream
                .range(0, str.length())
                .mapToObj(x -> new StringBuilder(str).replace(x, x + 1, String.valueOf(str.charAt(x)).toUpperCase()).toString())
                .filter(x -> !x.equals(str))
                .toArray(String[]::new);
    }

    public static String[] wave3(String str) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch == ' ') continue;
            list.add(str.substring(0,i) + Character.toUpperCase(ch) + str.substring(i+1));
        }
        for (String s:list
             ) {
            System.out.println(s);

        }
        return list.toArray(new String[0]);
    }




}
//if (Character.isLowerCase(str.charAt(j)))
