package kata6;

public class Pangram {

    public boolean check(String sentence) {
        if (sentence.length() < 26) {
            return false;
        }

        for (char c = 'A'; c <= 'Z'; c++) {
            if ((sentence.indexOf(c) < 0) && (sentence.indexOf((char) (c + 32)) < 0)) {
                return false;
            }
        }

        return true;


    }


}
