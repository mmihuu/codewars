package kata6;

import java.util.ArrayList;
import java.util.Arrays;

public class Persistance {

    public static int persistence(long n) {
        int index = 1;
        int count = 0;
        String templon = String.valueOf(n);
        if (templon.length() == 1) {
            return 0;
        }
        for (int j = 0; j < index; j++) {
            long temp = 1;
            String s = String.valueOf(n);
            char[] digits = s.toCharArray();
            if (digits.length <= 1) {
                break;
            }
            for (int i = 0; i < digits.length; i++) {
                //if (Character.getNumericValue(digits[i]) != 0) {
                long digit = Character.getNumericValue(digits[i]);
                temp *= digit;
                //}
            }
            n = temp;
            count++;
            index++;
        }
        return count;
    }

    public static int presistenceUpgraded(long n) {

        String[] split = String.valueOf(n).split("");


        return -1;
    }


    public static int persistence1(long n) {
        long m = 1, r = n;

        if (r / 10 == 0)
            return 0;

        for (r = n; r != 0; r /= 10)
            m *= r % 10;

        return persistence1(m) + 1;

    }

    public static int persistence3(long n) {
        if (Long.toString(n).length() < 2) {
            return 0;
        }
        return persistence3(Arrays.stream(Long.toString(n)
                .split(""))
                .mapToLong(Long::parseLong)
                .reduce(1L, (a, b) -> a * b)) + 1;
    }

}




