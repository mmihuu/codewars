package kata6;

import java.util.Arrays;

public class WriteNumberInExpandedForm {

    public static String expandedForm(int sum) {

        String[] s = Integer.toString(sum).split("");

        for ( int i = 0; i < s.length-1;i++){
            for( int j = i; j < s.length-1; j++){
                if(Integer.valueOf(s[i])>0)
                s[i]+= "0";
            }
        }
        String result = Arrays.toString(s);
        result= result.substring(1,result.length()-1).replace(", 0","").replace(", ","+");

        System.out.println(result);

        return result;
    }

    public static String expandedForm1(int num)
    {
        String outs = "";
        for (int i = 10; i < num; i *= 10) {
            int rem = num % i;
            outs = (rem > 0) ? " + " + rem + outs : outs;
            num -= rem;
        }
        outs = num + outs;

        System.out.println(outs);
        return outs;
    }


}
