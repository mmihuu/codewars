package kata5;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

public class IsMyFriendCheating {


    //brute force approach
    public static List<long[]> numberSequenceSummary(long a) {

        long total = LongStream.rangeClosed(0, a)
                .sum();
        long[] longs = LongStream.range(0, a).toArray();
        ArrayList<long[]> listOfNumbers = new ArrayList<long[]>();
        int counter = 0;




        for (long i = 0; i <= a; i++) {
            for (long j = 0; j <= a; j++) {

                if (i * j == total - i - j) {
                    long[] arrayOfLongs = new long[2];

                    arrayOfLongs[0] = i;
                    arrayOfLongs[1] = j;
                    listOfNumbers.add(counter,arrayOfLongs);
                    counter++;
                }


            }

        }


        return listOfNumbers;
    }



    public static List<long[]> removNb(long n) {

        List<long[]> pairsOfLongs = new ArrayList<>();

        long total = n * (n + 1) / 2;
        long lowerBound = ((n - 1) * n / 2) / (n + 1);
        long upperBound = (long) Math.sqrt(total + 1) - 1;
        for (long i = upperBound; i >= lowerBound; i--) {
            long b = (total - i)/(i + 1);
            if (i * b + i + b == total) {
                pairsOfLongs.add(new long[]{i, b});
                pairsOfLongs.add(new long[]{b, i});

            }
        }
        return pairsOfLongs;
    }


}
