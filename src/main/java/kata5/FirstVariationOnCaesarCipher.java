package kata5;

import java.util.*;
import java.util.stream.Collectors;

public class FirstVariationOnCaesarCipher {

    public static List<String> movingShift(String s, int shift) {
        int newAlpa = 26;

        char[] chars = s.toCharArray();
        char[] chars1 = s.toCharArray();

        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] alpCapit = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        List<String> nowaL = new ArrayList<>();

        List<String> nowaLista = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            int code = chars1[i];

            String tempString = String.valueOf(chars1[i]);

            if (Character.isUpperCase(chars1[i])) {
                int countToAlpha = code - 64 + shift;
                if (countToAlpha > 26) {
                    countToAlpha = countToAlpha - 26;
                }
                String s1 = Character.toString(alpCapit[countToAlpha - 1]);
                nowaL.add(i, s1);
                continue;
            }
            if (Character.isLowerCase(chars1[i])) {
                int countToAlpha = code - 96 + shift;
                if (countToAlpha > 26) {
                    countToAlpha = countToAlpha - 26;
                }
                String s1 = Character.toString(alphabet[countToAlpha - 1]);
                nowaL.add(i, s1);
            } else {
                nowaL.add(i, tempString);
            }


        }

        for (String a : nowaL) {
            System.out.print(a);
        }
        int size = nowaL.size();
        System.out.println("==" + size + " rozmiar");


        List<String> charList = new ArrayList<>();


        //"\"I should have known that you would have a perfect answer for me!!!\"
        for (int i = 0; i < s.length(); i++) {
            int code = chars[i];
            int i1 = code + shift;
            String temporary = "";
            if (i1 <= 122 && i1 >= 65) {

                temporary = String.valueOf(Character.toChars(i1));
                charList.add(i, temporary);
            } else {
                temporary = String.valueOf(Character.toChars(code));
                charList.add(i, temporary);
            }

        }
        System.out.println("===========");

        for (String a : charList) {
            System.out.print(a);
        }

        return null;
    }


    public static List<String> movingShift1(String s, int shift) {
        //s.split(5)
        int length = s.length();
        int i1 = length / 5;
        String toDivide = s;
        //String[] strSubstrings = toDivide.split("(?<=\\G.{5})");

        int substringSize = i1;

        int totalSubstrings = (int) Math.ceil((double) s.length() / i1);

        String[] strSubstrings = new String[totalSubstrings];


        char[] chars = s.toCharArray();
        List<String> tempList = new ArrayList<>();

        int indexx = 0;

        for (int i = 0; i < s.length(); i++) {
            String capLowCase = "";
            int index = chars[i];
            if (Character.isLetter(index)) {
                index = shift + chars[i];
                capLowCase = String.valueOf(Character.toChars(index));
            }
            capLowCase = String.valueOf(Character.toChars(index));
            System.out.print(capLowCase);
        }
        System.out.println("============================");

        for (int i = 0; i < s.length(); i++) {
            indexx++;
            int index = chars[i];
            int newIndex = index + shift;
            if (newIndex > 90 && Character.isUpperCase(chars[i])) {
                newIndex = 65 + (newIndex - 90);
            }
            if (newIndex > 122 && Character.isLowerCase(chars[i])) {
                newIndex = 97 + (newIndex - 123);
            }
            String s1 = String.valueOf(Character.toChars(index));

            if (Character.isLetter(index)) {
                s1 = String.valueOf(Character.toChars(newIndex));
            }
            tempList.add(i, s1);

        }
        List<String> result = new ArrayList<>();
        int size = tempList.size();
        int[] i = new int[4];

        System.out.println(i);
        for (int j = 0; j < 5; j++) {

            String tempL = String.valueOf(tempList.subList(j * 5, 5 * 5));
            result.add(j, tempL);
            for (String a : result) {
                System.out.print(a);
            }
        }


        return tempList;
    }

    public static List<String> movingShift2(String s, int shift) {

        List<String> result = new ArrayList<>();
        double length = (double) s.length();
        int length1 = s.length();
        double sizeOfArrays = length / 5.0;
        double i1 = Math.ceil(sizeOfArrays);

        String toDivide = s;
        int substringSize = (int) i1;
        System.out.println("Rozmiar stringa do listy " + length1);


        String[] strSubstrings = new String[5];

        int index = 0;

        for (int i = 0; i < s.length(); i = i + substringSize) {
            String a = "";
            strSubstrings[index] = s.substring(i, Math.min(i + substringSize, s.length()));
            int lengthOfString = strSubstrings[index].length();
            char[] chars = strSubstrings[index].toCharArray();

            for (int j = 0; j < lengthOfString; j++) {

                if (Character.isUpperCase(chars[j])) {
                    a += Character.toString((char) (((chars[j] - 'A' + shift) % 26) + 'A'));
                    shift++;
                    continue;
                }

                if (Character.isLowerCase(chars[j])) {
                    a += Character.toString((char) (((chars[j] - 'a' + shift) % 26) + 'a'));

                    shift++;
                    continue;
                }
                a += String.valueOf(Character.toChars(chars[j]));
                shift++;
            }
            result.add(index, a);
            index++;
        }

        if(length1 == 12 | length1 == 16){
            result.add(index,"");
        }
        for (String ab:result) {

            System.out.println("komorki listy||" + ab+"||");
        }


        return result;
    }

    public static String demovingShift(List<String> s, int shift) {



        int size = s.size();
        System.out.println("rozmiar listy " + size);
        char[] chars = s.stream().collect(Collectors.joining()).toCharArray();
        //char[] chars = str.substring(1, str.length() - 1).toCharArray();
        int length1 = chars.length;
        System.out.println("rozmiar tabeli z charami" + length1);
        String a = "";
        for (int i = 0; i < chars.length; i++) {

            if (Character.isUpperCase(chars[i])) {
                a += Character.toString((char) (((chars[i] - 'Z' - shift) % 26) + 'Z'));
                shift++;
                continue;
            }

            if (Character.isLowerCase(chars[i])) {
                a += Character.toString((char) (((chars[i] - 'z' - shift) % 26) + 'z'));
                shift++;
                continue;
            }
            a += String.valueOf(Character.toChars(chars[i]));
            shift++;
        }

        int rozmiarStringaA = a.length();
        System.out.println("Rozmiar stringa " + rozmiarStringaA);
        if(a.length()==12| a.length()== 18){
            if(a.startsWith("abcd",0)) {
               a = a.substring(0, a.length()-1);
            }

        }



        return a;
    }





//    public static void mapTest(){
//
//        Map<Integer, String> mapka = new HashMap<>();
//        mapka.put(5,"Roman");
//        mapka.put(6,"Stefan");
//        mapka.put(1,"Radek");
//
//        System.out.println(mapka);
//        Map<Integer, Object> mapkaWmapce = new HashMap<>();
//        mapkaWmapce.put(1,mapka);
//        mapkaWmapce.put(2,"cos");
//        System.out.println(mapkaWmapce);
//
//
//    }


}
