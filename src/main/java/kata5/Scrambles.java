package kata5;

import java.util.ArrayList;

public class Scrambles {

    public static boolean scramble(String str1, String str2) {

        StringBuilder sb = new StringBuilder();
        ArrayList<String> ar = new ArrayList<String>(str2.length());
        String tempString = str1;


        for (int i = 0; i < str2.length(); i++) {
            String ab = String.valueOf(str2.charAt(i));
            for (int j = 0; j < tempString.length(); j++) {

                String abc = String.valueOf(tempString.charAt(j));

                if (ab.contains(abc)) {
                    ar.add(ab);
                    tempString = tempString.replaceFirst(ab, "");
                    break;
                }
            }
        }
        for (String s : ar) {
            sb.append(s);
        }

        String s = sb.toString();


        if (s.contains(str2)) {
            return true;
        }


        return false;
    }


    public static boolean scramble4(String str1, String str2) {
        if (str2.length() > str1.length()) return false;
        for (String s : str2.split("")) {
            if (!str1.contains(s)) return false;
            str1 = str1.replaceFirst(s, "");
        }

        return true;
    }


}


//     if (sentence.length() < 26) {
//        return false;
//    }
//
//        for (char c = 'A'; c <= 'Z'; c++) {
//        if ((sentence.indexOf(c) < 0) && (sentence.indexOf((char) (c + 32)) < 0)) {
//            return false;
//        }
//    }
//
//        return true;


//    Complete the function scramble(str1, str2) that returns true if a portion of str1
//        characters can be rearranged to match str2, otherwise returns false.