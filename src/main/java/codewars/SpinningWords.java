package codewars;

import java.util.ArrayList;
import java.util.List;

public class SpinningWords {

    //    Write a function that takes in a string of one or more words,
    //    and returns the same string, but with all five or more letter
    //    words reversed (Just like the name of this Kata).
    //    Strings passed in will consist of only letters and spaces.
    //    Spaces will be included only when more than one word is present.


    public String zakreconeWyrazy(String sentence) {

        String[] str = sentence.split(" ");
        StringBuilder sB = new StringBuilder();


        for (int i = 0; i < str.length; i++) {
            String measure = str[i];

            if (measure.length() >= 5) {
                String tempStr = str[i];
                StringBuilder stringBuilder = new StringBuilder(tempStr);
                String reverseString = stringBuilder.reverse().toString();
                sB.append(reverseString).append(" ");


            } else {
                String tempStr = str[i];
                StringBuilder stringBuilder = new StringBuilder(tempStr);
                String normalString = stringBuilder.toString();
                sB.append(normalString).append(" ");

            }

        }

        if (sB.length() == 1) {
            sB.append("");
        }
        String s = sB.toString();
        String afterTrim = s.replaceAll("\\s+$", "");//usuwa spacje na koncu
        System.out.println(afterTrim);

        return afterTrim;
    }


}



