package codewars;


import java.util.*;

public class YourOdersPlease {


    public static String order(String words) {


        if (words.length() >= 1) {
            String[] arr = words.split(" ");


            int lengthOfArr = arr.length;

            List<String> linkedList = new LinkedList<>();
            for (int a = 0; a < lengthOfArr; a++) {
                linkedList.add(a, "");
            }
            for (int i = 0; i < linkedList.size(); i++) {

                int index = Integer.parseInt(arr[i].replaceAll("\\D+", ""));

                linkedList.set(index - 1, arr[i]);


                System.out.println(linkedList);
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (String s : linkedList) {
                stringBuilder.append(s);
                stringBuilder.append(" ");

            }

            int lastIndex = stringBuilder.lastIndexOf(" ");
            stringBuilder.deleteCharAt(lastIndex);
            return stringBuilder.toString();

        }
        String emptryString = "";
        return emptryString;

    }

    public static String order1(String words) {
        return Arrays.stream(words.split(" "))
                .sorted(Comparator.comparing(s -> Integer.valueOf(s.replaceAll("\\D", ""))))
                .reduce((a, b) -> a + " " + b).get();
    }
}


//temp = temp.replaceAll("\\B|\\b", ",");