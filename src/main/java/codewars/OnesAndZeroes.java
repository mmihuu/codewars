package codewars;

import java.util.Iterator;
import java.util.List;

public class OnesAndZeroes {

    public static int ConvertBinaryArrayToInt(List<Integer> binary) {

        StringBuilder sb = new StringBuilder();

        Iterator<Integer> iter = binary.iterator();
        while (iter.hasNext()) {
            sb.append(iter.next());
        }
        String s = sb.toString();

        Integer i = Integer.parseInt(s, 10);



        return i;


    }


    public static int ConvertBinaryArrayToIntSimple(List<Integer> binary) {
        return binary.stream().reduce((x, y) -> x * 2 + y).get();
    }
}



