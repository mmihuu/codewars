package codewars;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OddArray {


    public static int oddArray(int... a) {


        int flag = 0;
        int count = 0;
        int thisNumber = 0;


        for (int i = 0; i < a.length; i++) {


            for (int j = 0; j < a.length; j++) {

                if (i != j) {

                    if (a[i] != a[j]) {

                        flag = 1;
                    } else {
                        flag = 0;
                        break;

                    }


                }
            }
            if (flag == 1) {

                count++;
                System.out.println((a[i] + ""));
                thisNumber = a[i];

            }
        }


     return thisNumber;
    }

}

//       You are given an odd-length array of integers, in which all of them are the same, except for one single number.
//
//        Complete the method which accepts such an array, and returns that single different number.
//
//        The input array will always be valid! (odd-length >= 3)