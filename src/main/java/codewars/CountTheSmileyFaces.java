package codewars;

import com.sun.org.apache.bcel.internal.generic.Select;

import java.util.*;
import java.util.stream.Collectors;

public class CountTheSmileyFaces {


    public static int countSmileys(List<String> arr) {

        List<String> result = arr.stream()
                .filter(smileFace ->":)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result1 = arr.stream()
                .filter(smileFace ->":D".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result2 = arr.stream()
                .filter(smileFace ->":~)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result3 = arr.stream()
                .filter(smileFace ->";-D".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result4 = arr.stream()
                .filter(smileFace -> ";-)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result5 = arr.stream()
                .filter(smileFace -> ":~D".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result6 = arr.stream()
                .filter(smileFace -> ";)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result7 = arr.stream()
                .filter(smileFace -> ";D".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result8 = arr.stream()
                .filter(smileFace -> ":-)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result9 = arr.stream()
                .filter(smileFace -> ";~)".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result10 = arr.stream()
                .filter(smileFace -> ":-D".equals(smileFace))
                .collect(Collectors.toList());
        List<String> result11 = arr.stream()
                .filter(smileFace -> ";~D".equals(smileFace))
                .collect(Collectors.toList());

        int i = result.size() + result1.size() + result2.size() + result3.size()+result4.size()+result5.size()+result6.size()
                +result7.size()+result8.size()+result9.size()+result10.size()+result11.size();


        return i;



    }

    public static int countSmileys1(List<String> arr) {

        List<String> list = new ArrayList<>(arr);
        list.removeIf(s -> !s.contains(":)"));
        List<String> list1 = new ArrayList<>(arr);
        list.removeIf(s -> !s.contains(":D"));
        List<String> list2 = new ArrayList<>(arr);
        list.removeIf(s -> !s.contains(";-D"));
        List<String> list3 = new ArrayList<>(arr);
        list.removeIf(s -> !s.contains(":~)"));
        List<String> list4 = new ArrayList<>(arr);
        list.removeIf(s -> !s.contains(";-)"));



        int size = list.size() + list1.size()+ list2.size()+list3.size()+list4.size();


        return size;
    }



    public static int countSmileys3(List<String> arr) {
            return (int)arr.stream().filter( x -> x.matches("[:;][-~]?[)D]")).count();


        }





}


//.filter(smileFace ->":~)".equals(smileFace))
//                  .filter(smileFace ->":D".equals(smileFace))
//                  .filter(smileFace ->";-D".equals(smileFace))
//
//
//


//    Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.
//
//    Rules for a smiling face:
//            -Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
//-A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
//            -Every smiling face must have a smiling mouth that should be marked with either ) or D.
//    No additional characters are allowed except for those mentioned.
//    Valid smiley face examples:
//            :) :D ;-D :~)
//    Invalid smiley faces:
//    ;( :> :} :]





