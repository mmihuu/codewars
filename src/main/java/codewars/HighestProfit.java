package codewars;

import java.util.*;
import java.util.stream.Collectors;

public class HighestProfit {


    public HighestProfit(Integer maxNumber, Integer minNumber) {



    }

    public static HighestProfit highestProfit(List<Integer> a) {


        Integer maxNumber = a.stream().max(Comparator.comparing(Integer::valueOf))
                .get();

        Integer minNumber = a.stream().min(Comparator.comparing(Integer::valueOf)).get();



        System.out.println("minimum values is " + minNumber + " maximum value is " + maxNumber);


        return new HighestProfit(maxNumber,minNumber);
    }



    public static int[] minMax(int[] arr){

        int[] temp = new int[2];

        int min = Arrays.stream(arr).min().getAsInt();

        int max = Arrays.stream(arr).max().getAsInt();


        temp[0] = min;
        temp[1] = max;

        return temp;


    }


}
//Story
// Ben has a very simple idea to make some profit: he buys something and sells it again. Of course, this wouldn't give him any profit at all if he was simply to buy and sell it at the same price. Instead, he's going to buy it for the lowest possible price and sell it at the highest.
//
// Task
// Write a function that returns both the minimum and maximum number of the given list/array.