package codewars;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class RomanNumerals {

    private static final TreeMap<Integer,String> mapofRomanNumerals = new TreeMap<Integer, String>();

    static {

        mapofRomanNumerals.put(1000, "M");
        mapofRomanNumerals.put(900, "CM");
        mapofRomanNumerals.put(500, "D");
        mapofRomanNumerals.put(400, "CD");
        mapofRomanNumerals.put(100, "C");
        mapofRomanNumerals.put(90, "XC");
        mapofRomanNumerals.put(50, "L");
        mapofRomanNumerals.put(40, "XL");
        mapofRomanNumerals.put(10, "X");
        mapofRomanNumerals.put(9, "IX");
        mapofRomanNumerals.put(5, "V");
        mapofRomanNumerals.put(4, "IV");
        mapofRomanNumerals.put(3, "III");
        mapofRomanNumerals.put(2, "II");
        mapofRomanNumerals.put(1, "I");

    }


    public String solution(int n){



        return "";
    }

    public final static String toRoman(int number) {

        int l =  mapofRomanNumerals.floorKey(number);
        if ( number == l ) {
            return mapofRomanNumerals.get(number);
        }
        return mapofRomanNumerals.get(l) + toRoman(number-l);
    }

}












//    Create a function taking a positive integer as its parameter and returning a string containing the Roman Numeral representation of that integer.
//
//        Modern Roman numerals are written by expressing each digit separately
//        starting with the left most digit and skipping any digit with a value of zero.
//        In Roman numerals 1990 is rendered: 1000=M, 900=CM, 90=XC; resulting in MCMXC.
//        2008 is written as 2000=MM,
//        8=VIII; or MMVIII. 1666 uses each Roman symbol in descending order: MDCLXVI.